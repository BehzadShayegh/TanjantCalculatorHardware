module tan_reg(input [15:0] t, input tan_load, clk, rst, output reg [15:0] tan);
	always @(posedge clk , posedge rst) begin
		if(rst) tan = 16'b 0;
		else if(tan_load) tan = t;
		else tan = tan;
	end
endmodule