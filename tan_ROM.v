module tan_ROM (input [3:0] i, output reg [15:0] coefficient);
	always begin
		coefficient = 8'b 0;
		case (i)
			0: coefficient = 8'b 1;
			1: coefficient = 0101010101010101;
			2: coefficient = 0010001000100010;
			3: coefficient = 0000110111010000;
			4: coefficient = 0000010110011001;
			5: coefficient = 0001011010110000;
			6: coefficient = 0000000011101011;
			7: coefficient = 0000000001011111;
			default: coefficient = 8'b 1;
		endcase
	end
endmodule
